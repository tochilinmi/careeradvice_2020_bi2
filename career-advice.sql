-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 20 2020 г., 22:52
-- Версия сервера: 5.5.39
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `career-advice`
--

-- --------------------------------------------------------

--
-- Структура таблицы `coworkings`
--

CREATE TABLE IF NOT EXISTS `coworkings` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) unsigned DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `coworkings`
--

INSERT INTO `coworkings` (`id`, `title`, `text`, `address`, `price`, `creator_id`) VALUES
(1, 'Коворкинг 1', 'текст коворкинга 1', 'адрес коворкинга 1', 500, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `issue`
--

CREATE TABLE IF NOT EXISTS `issue` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `employee_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `issue`
--

INSERT INTO `issue` (`id`, `title`, `description`, `date`, `employee_id`, `project_id`) VALUES
(1, 'Задача 2', 'Описание задачи 2', '2020-01-31', 2, 1),
(2, 'Помириться', 'Быть дружелюбным', '2020-06-19', 1, 4),
(3, 'Помириться2', 'Быть дружелюбным', '2020-06-19', 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `menedgers`
--

CREATE TABLE IF NOT EXISTS `menedgers` (
`id` int(11) unsigned NOT NULL,
  `position_user_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menedgers`
--

INSERT INTO `menedgers` (`id`, `position_user_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`) VALUES
(1, 'Новость 1', 'текст новости 1'),
(2, 'Презентация проекта', 'Приходите'),
(3, 'Презентация проекта', 'Приходите'),
(4, 'Презентация проекта', 'Приходите сегодня');

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
`id` int(11) unsigned NOT NULL,
  `position_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`id`, `position_type`) VALUES
(1, 'Разработчик'),
(2, 'Веб-разработчик'),
(3, 'Администратор'),
(4, 'Недруг');

-- --------------------------------------------------------

--
-- Структура таблицы `positions_users`
--

CREATE TABLE IF NOT EXISTS `positions_users` (
`id` int(11) unsigned NOT NULL,
  `positions_id` int(11) unsigned DEFAULT NULL,
  `users_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `positions_users`
--

INSERT INTO `positions_users` (`id`, `positions_id`, `users_id`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 4),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `positions_users_teams`
--

CREATE TABLE IF NOT EXISTS `positions_users_teams` (
`id` int(11) unsigned NOT NULL,
  `positions_users_id` int(11) unsigned DEFAULT NULL,
  `teams_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `positions_users_teams`
--

INSERT INTO `positions_users_teams` (`id`, `positions_users_id`, `teams_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 4, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(11) unsigned NOT NULL,
  `proj_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(11) unsigned DEFAULT NULL,
  `team_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `proj_name`, `description`, `creator_id`, `team_id`) VALUES
(1, 'Career advice', 'Медиаплатформа, которая объединяет школьников, студентов и работодателей через проектную деятельность.', 1, 1),
(2, 'Дружба', 'Крепкая', 1, 2),
(3, 'Знакомство', 'Общение', 1, 3),
(4, 'Дружба2', 'Странная', 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
`id` int(11) unsigned NOT NULL,
  `team_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `teams`
--

INSERT INTO `teams` (`id`, `team_name`) VALUES
(1, 'Career advice'),
(2, 'Дружба'),
(3, 'Знакомство'),
(4, 'Дружба2');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `snd_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fth_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `snd_name`, `name`, `fth_name`, `city`, `phone`, `password`) VALUES
(1, 'misha', '2@mail.ru', 'Точилин', 'Михаил', '-', '-', '-', '1'),
(2, 'gul-kat', 'gul-kat@mail.ru', 'Гуляева', 'Екатерина', '-', '-', '-', '1'),
(3, 'rat-dar', 'rat-dar@mail.ru', 'Ратманова', 'Дарья', '-', '-', '-', '$2y$10$kgXf1kiCKBwjSIffW7JBeusSw.ojGIO9UH2SAQalRekRd7SN0v4AG'),
(4, 'admin', 'd21@mail.ru', '-', 'admin', '-', '-', '-', '1'),
(5, '1', '1@mail.c', '1', '1', '1', '1', '1', '1'),
(6, '2', '21@mail.c', '1', '1', '1', '1', '1', '2'),
(7, '3', '3@mail.c', '1', '1', '1', '1', '1', '3'),
(8, '31', '31@mail.c', '1', '1', '1', '1', '1', '31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coworkings`
--
ALTER TABLE `coworkings`
 ADD PRIMARY KEY (`id`), ADD KEY `index_foreignkey_coworkings_creator` (`creator_id`);

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
 ADD PRIMARY KEY (`id`), ADD KEY `index_foreignkey_issue_employee` (`employee_id`), ADD KEY `index_foreignkey_issue_project` (`project_id`);

--
-- Indexes for table `menedgers`
--
ALTER TABLE `menedgers`
 ADD PRIMARY KEY (`id`), ADD KEY `index_foreignkey_menedgers_position_user` (`position_user_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions_users`
--
ALTER TABLE `positions_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UQ_878867b90d62e8acfaf87d086ed2a5784f0e318b` (`positions_id`,`users_id`), ADD KEY `index_foreignkey_positions_users_positions` (`positions_id`), ADD KEY `index_foreignkey_positions_users_users` (`users_id`);

--
-- Indexes for table `positions_users_teams`
--
ALTER TABLE `positions_users_teams`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UQ_41b395d0fcd0b471092cdef855e26aee03332526` (`positions_users_id`,`teams_id`), ADD KEY `index_foreignkey_positions_users_teams_positions_users` (`positions_users_id`), ADD KEY `index_foreignkey_positions_users_teams_teams` (`teams_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD KEY `index_foreignkey_projects_creator` (`creator_id`), ADD KEY `index_foreignkey_projects_team` (`team_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coworkings`
--
ALTER TABLE `coworkings`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `issue`
--
ALTER TABLE `issue`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `menedgers`
--
ALTER TABLE `menedgers`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `positions_users`
--
ALTER TABLE `positions_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `positions_users_teams`
--
ALTER TABLE `positions_users_teams`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `coworkings`
--
ALTER TABLE `coworkings`
ADD CONSTRAINT `c_fk_coworkings_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `issue`
--
ALTER TABLE `issue`
ADD CONSTRAINT `c_fk_issue_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `c_fk_issue_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `menedgers`
--
ALTER TABLE `menedgers`
ADD CONSTRAINT `c_fk_menedgers_position_user_id` FOREIGN KEY (`position_user_id`) REFERENCES `positions_users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `positions_users`
--
ALTER TABLE `positions_users`
ADD CONSTRAINT `c_fk_positions_users_positions_id` FOREIGN KEY (`positions_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `c_fk_positions_users_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `positions_users_teams`
--
ALTER TABLE `positions_users_teams`
ADD CONSTRAINT `c_fk_positions_users_teams_positions_users_id` FOREIGN KEY (`positions_users_id`) REFERENCES `positions_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `c_fk_positions_users_teams_teams_id` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `projects`
--
ALTER TABLE `projects`
ADD CONSTRAINT `c_fk_projects_creator_id` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `c_fk_projects_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
