﻿<?php 
session_start();
if (isset($_SESSION['user'])) {
  $login = $_SESSION['user']['login'];
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Career Advice</title>
  <link rel="stylesheet" href="style.css">
</head>
 <?php include 'header.php' ?>
<body class="bgcg">
   <div>
    <img  src="img/2.png" class="el-x-1">
    <h1 class="text-h1-vid" >Возможность попробовать будущее уже сегодня</h1>
    <p class="text-p-vid">Мы подберём вам проект и найдём команду</p>
    <img  src="img/1.png" class="el-p-1">
    <a href="projects.php" class="btn-a1">Выбрать проект</a>
    <img  src="img/3.png" class="el-o-1">
  </div>

  <div class="vid-cont">
    <p class="img-vid"></p>
    <video class="cv" autoplay loop>
      <source src="video1.mp4" type="video/mp4">
    </video>  
  </div>

  <nav class="utp">

    <div class="utp-c1">
      <div class="utp-c1-img-cont">
        <img src="img/ik-p/s1.png" class="utp-c1-img">
      </div>
      <h2 class="utp-seg">АБИТУРИЕНТЫ</h2>
        <p class="utp-text">На нашей платформе вы уже начнете работать с лидерами рынка. HR-специалисты и руководители компаний познакомят вас с востребованными профессиями и расскажут, какое будущее ждет специалистов.</p>
       </div>
    </div>  
    
    <div class="utp-c1">
      <div class="utp-c1-img-cont">
        <img src="img/ik-p/st1.png" class="utp-c1-img">
      </div>
      <h2 class="utp-seg">СТУДЕНТЫ</h2>
      <p class="utp-text">После участия в наших программах молодые специалисты получают приглашения в лучшие компании на высокооплачиваемые должности или получают карьерный рост, приходя в компанию ещё на первых курсах.</p>
    </div>
    
    
    <div class="utp-c1">
      <div class="utp-c1-img-cont">
        <img src="img/ik-p/r1.png" class="utp-c1-img">
      </div>
      <h2 class="utp-seg">РУКОВОДИТЕЛИ КОМПАНИЙ</h2>
      <p class="utp-text">Поколение Z разрушает рамки стереотипов о молодом специалисте. Уже в выпускных классах они участвуют в кейс-чемпионатах и решают задачи компаний, успешно занимающих позицию лидера рынка на протяжении многих лет.</p>
      </div>
    </div>  
  </nav>

  <div class="vstavka">
    <h4 class="h4-vs">Career Advice</h4>
    <h1 class="h1-vs">Изменит вашу жизнь к лучшему</h1>

    <img src="img/4.png" class="img4-vs">
    <img src="img/5.png" class="img5-vs">  
  </div>

  <div class="partners-conteiner">
      <a class="pertn-cont-img">
        <img src="img/partners/1.png" class="partn-img">
      </a >
      <a class="pertn-cont-img">
        <img src="img/partners/2.png" class="partn-img">
      </a >
      <a class="pertn-cont-img">
        <img src="img/partners/3.png" class="partn-img">
      </a >
      <a class="pertn-cont-img">
        <img src="img/partners/4.png" class="partn-img">
      </a >
      <a class="pertn-cont-img">
        <img src="img/partners/5.png" class="partn-img">
      </a >
  </div>
<div class="foot-cont" style="display: block;">
    <div class="foot-social-cont">
      <a href="fb.com"><img src="img/social/2.webp" class="foot-img"></a>
      <a href="inst.com"><img src="img/social/3.webp" class="foot-img"></a>
      <a href="vk.com"><img src="img/social/1.webp" class="foot-img"></a>
    </div>
    <p class="foot-text">© 2020 Career Advice</p>
</div>
</body>
</html>

