﻿<?php require "header.php"; ?>
	<div class="bg-gradient">
	<?php 
		$user=$_SESSION['logged_user'];
		$num=$user['id'];
		$issuies=R::find('issue', 'employee_id = ?', [$num]);
		$count=R::count('issue');
		$i=1;
		#{"id":"1","title":"\u0417\u0430\u0434\u0430\u0447\u0430 2","description":"\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0437\u0430\u0434\u0430\u0447\u0438 2","date":"2020-01-31","employee_id":"2","project_id":"1"}

	 ?>

	 	<div style="padding-bottom: 100px;">
	 		<h1 class="title">Задачи</h1>
	 		<?php require "cab-left-menu.php";
	 			foreach ($issuies as $value):
	 				$projName=R::findOne('projects', 'id = ?', array($value['project_id']));
	 				if ($projName): ?>
	 				<div class="issue-cont">	
	 					<h2>
	 					<?php echo $projName['proj_name'];?>
		 				</h2>
		 				<h4><?php echo $value['title'];?></h4>
		 				<p><?php echo $value['description'];?></p>
		 				<p>Выполнить до: <?php echo $value['date'];?></p>
		 			</div>	
	 				<?php endif;
	 			endforeach;
	 		?>

	 	</div>

	</div>
<?php require "footer.php"; ?>
</body>
</html>