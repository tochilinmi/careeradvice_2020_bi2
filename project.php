﻿<?php include 'header.php' ?>
	
<div class="bg-gradient" style="padding-top: 70px">
	<?php require 'cab-left-menu.php' ?>

	<div class="descr-proj">
		<div class="proj-cont">
			<h1 class="title" style="color: #07123F;">
				<?php 
					$id = (int) $_GET['id'];
					$result = R::findOne('projects', 'id = ?', [$id]);
					echo $result->proj_name;	
				?>

			</h1>
			<p style="margin: 0 10% 0 10%;">
				<?php 	
					echo $result->description;	
				?>

			</p>
			
			<div class="team-list"><h2 class="ttc">Команда проекта</h2>
				<strong>Создатель: <a href="" class="list-a"> 
					<?php 
					// функция вывода имени участника проекта (Создателя)
						$creator = R::findOne('projects', 'id = ?', [$id]);
						$cr_name = R::findOne('users', 'id = ?', array($creator['creator_id']));
						echo $cr_name->name;
					 ?>
				</a></strong>
				
					<?php //вывод участников команды

					$put= R::find('positions_users_teams');
					$i=1;
						foreach ($put as $value) {
							// echo $value; -> {"id":"1","positions_users_id":"1","teams_id":"1"}
							//echo '1';
							if($value["teams_id"]==$id){
								$poUs= R::findOne('positions_users', 'id = ?', array($value['positions_users_id']));
								$pos = R::findOne('positions', 'id = ?', array($poUs['positions_id']));
								$emp_name = R::findOne('users', 'id = ?', array($poUs['users_id']));
								echo "<p>";
								echo $pos['position_type'];
								echo ": ";
								echo '<a href="" class="list-a">';
								echo $emp_name['name']; 
								echo "</a>";
								echo "</p>";

							}
							$i++;
						}?>

			</div>
				<div style="
				margin: 20px 0 0;
					text-align: center;">
				<?php  
				$a=$_SESSION['logged_user']->id;
				$b=$cr_name['id'];
					if ($a==$b) {

						require "fb2.html";
						echo 'Добавить исполнителя';
						require "fe.html";
						require "fb3.html";
						echo 'Добавить задачу';
						require "fe.html";
					}
				?>
				</div>
		</div>
	</div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
