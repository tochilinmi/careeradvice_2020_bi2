﻿<?php //функционал регистрации 
	require '../../db.php';

	$data =$_POST;
	if(isset($data['do_signup']))
	{
		//здесь регистрируем
		$errors = array();
		if( trim($data['proj_name'])=='')
		{
			$errors[]="Введите название";
		}

		if( trim($data['description'])=='')
		{
			$errors[]="Введите описание";
		}


		if (empty($errors)) 
		{
			

			# создаём команду
			$team = R::dispense('teams');
			$team->team_name = $data['proj_name']; //название команды = название проекта
			R::store($team);

			# создаём проект
			$project = R::dispense('projects');
			$project->proj_name = $data['proj_name'];
			$project->description = $data['description'];
			$project->creator = $_SESSION['logged_user']; //добавляем создателя проекта
			$project->team=$team;
			R::store($project);
			
			echo 
			'<div style="color: green;">
				Вы успешно зарегистрированы
				<meta http-equiv="Refresh" content="2; URL=../../project-reg.php">
			</div><hr>';
		

		} else
		{
			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
		}
	}

 ?>