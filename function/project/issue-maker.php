﻿<?php //функционал регистрации 
	require '../../db.php';

	$data=$_POST;
	$data2=$_SESSION['proj_id'];//project_id
	/*
	данные сущности:
		1. id
		2. title
		3. description
		4. employee_id
		5. date
		6. project_id

	*/

	// Проверить на ошибки (ввода и совпадения)
	// Связать пользователя и команду
if (isset($data['add_issue'])) {
		# авторизуемся
		$errors = array();
			
		if(trim($data['title'])=='') 			{$errors[]="Наименование задачи";}
		if(trim($data['description'])=='')		{$errors[]="Введите описание";}
		if(trim($data['login_employee'])=='')	{$errors[]="Введите логин исполнителя";}
		if(trim($data['date'])=='')				{$errors[]="Введите дату";}	

		$user = R::findOne('users', 'login = ?', array($data['login_employee']));
		if($user)//пользователь существует
		{
			$issue = R::dispense('issue');
			$issue->title=$data['title'];
			$issue->description=$data['description'];
			$issue->employee=$user;
			$issue->date=$data['date'];
			$issue->project=R::findOne('projects', 'id = ?', [$data2]);
			R::store($issue);

 			echo 
			'<div style="color: green;">
				Задача успешно добавлена
				<meta http-equiv="Refresh" content="2; URL=../../cab.php">
			</div><hr>';

		} else
		{
			$errors[]='Пользователь с таким логином не найден';
		}

		if (!empty($errors)) 
		{
			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
		}

	}
 ?>