﻿<?php 
	require '../db.php';

	$data=$_POST;

	if (isset($data['add_coworking'])) {

		$errors = array();
			
		if( trim($data['title'])=='')
			{
				$errors[]="Введите заголовок";
			}

		if( trim($data['text'])=='')
			{
				$errors[]="Введите текст";
			}

		if( trim($data['address'])=='')
			{
				$errors[]="Введите адрес";
			}

		if( trim($data['login'])=='')
			{
				$errors[]="Введите логин представителя";
			}

		if( trim($data['price'])=='')
			{
				$errors[]="Введите цену за 1 метр квадратный";
			}
	
			$creator=R::findOne('users', 'login = ?', array($data['login'])); 
			if ($creator) {
				//создаём таблицу с coworking
				$coworking = R::dispense('coworkings');
				$coworking->title = $data['title'];
				$coworking->text = $data['text'];
				$coworking->address = $data['address'];
				$coworking->creator = $creator;
				$coworking->price = $data['price'];
				R::store($coworking);
	 			echo 
				'<div style="color: green;">
					Coworking успешно добавлен
					<meta http-equiv="Refresh" content="2; URL=../coworking-maker.php">
				</div><hr>';
			} else
				{
					$errors[]='Пользователь с таким логином не найден';
				}

		if (!empty($errors)) 
		{
			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
		}

	}
?>