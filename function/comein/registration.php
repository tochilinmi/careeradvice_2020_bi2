﻿<?php //функционал регистрации 
	include '../../db.php';

	$data =$_POST;
	if(isset($data['do_signup']))
	{
		//здесь регистрируем
		$errors = array();
		if( trim($data['login'])=='')
		{
			$errors[]="Введите логин";
		}

		if( trim($data['email'])=='')
		{
			$errors[]="Введите email";
		}

		if( $data['password']=='')
		{
			$errors[]="Введите пароль";
		}

		if( $data['password_2'] != $data['password'])
		{
			$errors[]="Пароли не совпадают";
		}

		if( R::count('users', "login = ?", array($data['login'])) > 0)
		{
			$errors[]="Данный логин уже занят";
		}

		if( R::count('users', "email = ?", array($data['email'])) > 0)
		{
			$errors[]="Данный email уже занят";
		}

		if (empty($errors)) 
		{
			# регистрируем
			$user = R::dispense('users');
			$user->login = $data['login'];
			$user->email = $data['email'];
			$user->snd_name = $data['snd_name'];
			$user->name = $data['name'];
			$user->fth_name = $data['fth_name'];			
			$user->city = $data['city'];
			$user->phone = $data['phone'];
			$user->password = $data['password'];
			R::store($user);

			//регистрируем админа
			$admin='admin';
			if($data['login']==$admin){


				//создаём таблицу с должностями
				$position = R::dispense('positions');
				$position->position_type = 'Администратор';
				R::store($position);

				//создаём таблицу user_position	
				//создаём связи между таблицами
				
				$resultA=$user;				
				$resultB=$position;

				$resultA->sharedUser[] = $resultB;
				//store-> создать таблицу
				R::store($resultA); 

				//создаём таблицу управленцев
				$team=R::dispense('menedgers');
				$ps=R::findOne('positions_users', 'users_id = ?', array($user['id']));
				$team->position_user=$ps;
				R::store($team);


			}

			echo 
			'<div style="color: green;">
				Вы успешно зарегистрированы
				<meta http-equiv="Refresh" content="2; URL=../../auth.php">
			</div><hr>';
		} else
		{
			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
		}
	}

 ?>