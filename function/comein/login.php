﻿<?php 
	require '../../db.php';

	$data = $_POST;
	if (isset($data['do_login'])) {
		# авторизуемся
		$errors = array();
		$user = R::findOne('users', 'login = ?', array($data['login']));
		if($user)
		{
			//логин существует
			if($data['password'] == $user->password)
			{
				//логиним пользователя и через 2с переводим в личный кабинет
				$_SESSION['logged_user']=$user;
				echo 
				'<div style="color: green;">
					Вы авторизованы. <br>
					<meta http-equiv="Refresh" content="2; URL=../../cab.php">
				</div><hr>';
			}else
			{
				$errors[]='Введён неверный пароль';
			}
		} else
		{
			$errors[]='Пользователь с таким логином не найден';
		}

		if (!empty($errors)) 
		{
			echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
		}

	}
?>